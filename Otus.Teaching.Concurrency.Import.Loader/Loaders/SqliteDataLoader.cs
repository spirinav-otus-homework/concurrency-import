﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

using Otus.Teaching.Concurrency.Import.Core.Settings;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;
using Otus.Teaching.Concurrency.Import.Loader.Extensions;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
	public class SqliteDataLoader : IDataLoader
	{
		private readonly IEnumerable<Customer> _data;
		private readonly ILoaderSetting _loaderSetting;
		private readonly ICustomerRepository _customerRepository;
		private List<ManualResetEvent> manualResetEvents = new();

		public SqliteDataLoader(IEnumerable<Customer> data, ILoaderSetting loaderSetting,
			ICustomerRepository customerRepository)
		{
			_data = data;
			_loaderSetting = loaderSetting;
			_customerRepository = customerRepository;
		}

		public void LoadData()
		{
			var parts = _data.Split(_loaderSetting.ThreadCount);

			if (_loaderSetting.UseThreadPool)
			{
				LoadDataThreadPool(parts);
			}
			else
			{
				LoadDataThread(parts);
			}

			WaitHandle.WaitAll(manualResetEvents.ToArray());
		}

		private void LoadDataThread(IEnumerable<IEnumerable<Customer>> parts)
		{
			foreach (var p in parts)
			{
				var mre = new ManualResetEvent(false);
				manualResetEvents.Add(mre);

				new Thread(Insert).Start(new Tuple<IEnumerable<Customer>, ManualResetEvent>(p, mre));
			}
		}

		private void LoadDataThreadPool(IEnumerable<IEnumerable<Customer>> parts)
		{
			ThreadPool.SetMaxThreads(_loaderSetting.ThreadCount, _loaderSetting.ThreadCount);

			foreach (var p in parts)
			{
				var mre = new ManualResetEvent(false);
				manualResetEvents.Add(mre);

				ThreadPool.QueueUserWorkItem(Insert, new Tuple<IEnumerable<Customer>, ManualResetEvent>(p, mre));
			}
		}

		private void Insert(object data)
		{
			if (data is not Tuple<IEnumerable<Customer>, ManualResetEvent> tuple)
			{
				return;
			}

			var beginId = tuple.Item1?.OrderBy(x => x.Id).FirstOrDefault()?.Id;
			var endId = tuple.Item1?.OrderByDescending(x => x.Id).FirstOrDefault()?.Id;
			var threadId = Thread.CurrentThread.ManagedThreadId;

			Console.WriteLine($"Thread {threadId} started. Loading customers {beginId} - {endId}");
			foreach (var customer in tuple.Item1)
			{
				_customerRepository.AddCustomer(customer);
			}
			Console.WriteLine($"Thread {threadId} finished. Loaded customers {beginId} - {endId}");

			tuple.Item2.Set();
		}
	}
}