﻿using System.Collections.Generic;

using Otus.Teaching.Concurrency.Import.Core.Settings;
using Otus.Teaching.Concurrency.Import.DataAccess.Repositories;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.Core.Loaders
{
	public class LoaderFactory
	{
		private readonly ILoaderSetting _loaderSetting;

		public LoaderFactory(ILoaderSetting loaderSetting)
		{
			_loaderSetting = loaderSetting;
		}

		public IDataLoader GetLoader(IEnumerable<Customer> data, string connectionString)
		{
			if (_loaderSetting.UseDataBase)
			{
				return new SqliteDataLoader(data, _loaderSetting, new CustomerRepository(connectionString));
			}

			return new FakeDataLoader(data, _loaderSetting);
		}
	}
}