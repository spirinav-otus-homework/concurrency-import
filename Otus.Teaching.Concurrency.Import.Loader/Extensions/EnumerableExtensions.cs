﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.Concurrency.Import.Loader.Extensions
{
    public static class EnumerableExtensions
    {
        public static IEnumerable<IEnumerable<TData>> Split<TData>(this IEnumerable<TData> source, int partCount)
        {
            var partSize = (int) Math.Ceiling((double) source.Count() / partCount);
            while (source.Any())
            {
                yield return source.Take(partSize);
                source = source.Skip(partSize);
            }
        }
    }
}