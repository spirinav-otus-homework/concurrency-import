﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;

using ConsoleApp.Settings;

using Microsoft.Data.Sqlite;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.Core.Settings;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace ConsoleApp
{
	static class Program
	{
		private static IConfiguration _configuration;
		private static IFileSetting _fileSetting;
		private static IGeneratorSetting _generatorSetting;
		private static ILoaderSetting _loaderSetting;
		private static ServiceProvider _serviceProvider;

		static void Main()
		{
			InitConfiguration();
			ConfigureServices();
			InitCustomerTable();
			GenerateCustomers();
			var customers = ParseCustomers();

			var stopwatch = new Stopwatch();
			stopwatch.Start();
			LoadCustomers(customers);
			stopwatch.Stop();

			Console.WriteLine($"Count customers: {_generatorSetting.CountGenerateObject}. " +
			                  $"Count threads: {_loaderSetting.ThreadCount}. " +
			                  $"Load time: {stopwatch.Elapsed}");

			Console.ReadLine();
		}

		private static void InitConfiguration()
		{
			_configuration = new ConfigurationBuilder()
				.SetBasePath(AppDomain.CurrentDomain.BaseDirectory)
				.AddJsonFile("appsettings.json", false)
				.Build();

			var fileSetting = _configuration.GetSection("FileSetting");
			Enum.TryParse<FileType>(fileSetting.GetSection("FileType").Value, true, out var fileType);
			_fileSetting = new FileSetting(fileType, fileSetting.GetSection("FileName").Value);

			var generatorSetting = _configuration.GetSection("GeneratorSetting");
			bool.TryParse(generatorSetting.GetSection("RunInNewProcess").Value, out var runInNewProcess);
			int.TryParse(generatorSetting.GetSection("CountGenerateObject").Value, out var countGenerateObject);
			_generatorSetting = new GeneratorSetting(runInNewProcess, countGenerateObject);

			var loaderSetting = _configuration.GetSection("LoaderSetting");
			bool.TryParse(loaderSetting.GetSection("UseDataBase").Value, out var useDataBase);
			bool.TryParse(loaderSetting.GetSection("UseThreadPool").Value, out var useThreadPool);
			int.TryParse(loaderSetting.GetSection("ThreadCount").Value, out var threadCount);
			_loaderSetting = new LoaderSetting(useDataBase, useThreadPool, threadCount);
		}

		private static void ConfigureServices()
		{
			_serviceProvider = new ServiceCollection()
				.AddSingleton(_fileSetting)
				.AddSingleton(_generatorSetting)
				.AddSingleton(_loaderSetting)
				.AddSingleton<GeneratorFactory>()
				.AddSingleton<ParserFactory>()
				.AddSingleton<LoaderFactory>()
				.BuildServiceProvider();
		}

		private static void InitCustomerTable()
		{
			if (!_loaderSetting.UseDataBase)
			{
				return;
			}

			using var connection = new SqliteConnection(_configuration.GetConnectionString("Sqlite"));
			connection.Open();
			var command = new SqliteCommand
			{
				Connection = connection,
				CommandText = @"
					PRAGMA journal_mode = 'wal';
					CREATE TABLE IF NOT EXISTS customers (
						id INTEGER PRIMARY KEY,
						full_name TEXT,
						email TEXT,
						phone TEXT
					);"
			};
			command.ExecuteNonQuery();
		}

		private static void GenerateCustomers()
		{
			if (_generatorSetting.RunInNewProcess)
			{
				var appExePath = GetPath(@"Otus.Teaching.Concurrency.Import.DataGenerator.App", "exe");
				var args = $"{_fileSetting.FileName} {_fileSetting.FileType} {_generatorSetting.CountGenerateObject}";
				Console.WriteLine($"Run new process: {appExePath}");
				Process.Start(appExePath, args)?.WaitForExit();
				return;
			}

			var fileType = _fileSetting.FileType.ToString().ToLower();
			Console.WriteLine($"Generating {fileType} data in method...");
			_serviceProvider.GetRequiredService<GeneratorFactory>().GetGenerator().Generate();
			Console.WriteLine($"Generated {fileType} data in {GetPath(_fileSetting.FileName, fileType)}...");
		}

		private static IEnumerable<Customer> ParseCustomers()
		{
			return _serviceProvider.GetRequiredService<ParserFactory>().GetParser().Parse();
		}

		private static void LoadCustomers(IEnumerable<Customer> customers)
		{
			var connectionString = _configuration.GetConnectionString("Sqlite");
			_serviceProvider.GetRequiredService<LoaderFactory>().GetLoader(customers, connectionString).LoadData();
		}

		private static string GetPath(string fileName, string fileType)
		{
			return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, fileName + '.' + fileType);
		}
	}
}