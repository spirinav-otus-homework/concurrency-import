﻿using Otus.Teaching.Concurrency.Import.Core.Settings;

namespace ConsoleApp.Settings
{
	public class FileSetting : IFileSetting
	{
		public FileType FileType { get; set; }

		public string FileName { get; set; }

		private FileSetting()
		{
		}

		public FileSetting(FileType fileType, string fileName)
		{
			FileType = fileType;
			FileName = fileName;
		}
	}
}