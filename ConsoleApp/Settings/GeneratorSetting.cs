﻿using Otus.Teaching.Concurrency.Import.Core.Settings;

namespace ConsoleApp.Settings
{
	public class GeneratorSetting : IGeneratorSetting
	{
		public bool RunInNewProcess { get; set; }
		
		public int CountGenerateObject { get; set; }

		private GeneratorSetting()
		{
		}

		public GeneratorSetting(bool runInNewProcess, int countGenerateObject)
		{
			RunInNewProcess = runInNewProcess;
			CountGenerateObject = countGenerateObject;
		}
	}
}