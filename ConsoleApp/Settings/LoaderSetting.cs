﻿using Otus.Teaching.Concurrency.Import.Core.Settings;

namespace ConsoleApp.Settings
{
	public class LoaderSetting : ILoaderSetting
	{
		public bool UseDataBase { get; set; }

		public bool UseThreadPool { get; set; }

		public int ThreadCount { get; set; }

		private LoaderSetting()
		{
		}

		public LoaderSetting(bool useDataBase, bool useThreadPool, int threadCount)
		{
			UseDataBase = useDataBase;
			UseThreadPool = useThreadPool;
			ThreadCount = threadCount;
		}
	}
}