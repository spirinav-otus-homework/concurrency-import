﻿namespace Otus.Teaching.Concurrency.Import.Core.Settings
{
	public interface ILoaderSetting
	{
		public bool UseDataBase { get; set; }

		public bool UseThreadPool { get; set; }

		public int ThreadCount { get; set; }
	}
}