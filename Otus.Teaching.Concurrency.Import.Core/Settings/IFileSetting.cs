﻿namespace Otus.Teaching.Concurrency.Import.Core.Settings
{
	public interface IFileSetting
	{
		public FileType FileType { get; set; }

		public string FileName { get; set; }
	}

	public enum FileType
	{
		Xml,
		Csv
	}
}