﻿using System;

using Otus.Teaching.Concurrency.Import.Core.Settings;
using Otus.Teaching.Concurrency.Import.Handler.Data;

namespace Otus.Teaching.Concurrency.Import.DataGenerator.Generators
{
	public class GeneratorFactory
	{
		private readonly IFileSetting _fileSetting;
		private readonly IGeneratorSetting _generatorSettings;

		public GeneratorFactory(IFileSetting fileSetting, IGeneratorSetting generatorSettings)
		{
			_fileSetting = fileSetting;
			_generatorSettings = generatorSettings;
		}

		public IDataGenerator GetGenerator()
		{
			return _fileSetting.FileType switch
			{
				FileType.Xml => new XmlGenerator($"{_fileSetting.FileName}.xml",
					_generatorSettings.CountGenerateObject),
				FileType.Csv => new CsvGenerator($"{_fileSetting.FileName}.csv",
					_generatorSettings.CountGenerateObject),
				_ => throw new ArgumentOutOfRangeException()
			};
		}
	}
}