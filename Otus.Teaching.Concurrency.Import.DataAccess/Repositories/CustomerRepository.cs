using Microsoft.Data.Sqlite;

using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
	public class CustomerRepository
		: ICustomerRepository
	{
		private readonly string _connectionString;

		public CustomerRepository(string connectionString)
		{
			_connectionString = connectionString;
		}

		public void AddCustomer(Customer customer)
		{
			using var connection = new SqliteConnection(_connectionString);
			connection.Open();

			var command = new SqliteCommand
			{
				Connection = connection,
				CommandText = @"INSERT INTO customers (full_name, email, phone) VALUES ($full_name, $email, $phone);"
			};
			command.Parameters.AddWithValue("$full_name", customer.FullName);
			command.Parameters.AddWithValue("$email", customer.Email);
			command.Parameters.AddWithValue("$phone", customer.Phone);

			command.ExecuteNonQuery();
		}
	}
}