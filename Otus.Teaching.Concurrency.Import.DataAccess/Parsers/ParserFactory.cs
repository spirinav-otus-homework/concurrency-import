﻿using System;
using System.Collections.Generic;

using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Core.Settings;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
	public class ParserFactory
	{
		private readonly IFileSetting _fileSetting;

		public ParserFactory(IFileSetting fileSetting)
		{
			_fileSetting = fileSetting;
		}

		public IDataParser<List<Customer>> GetParser()
		{
			return _fileSetting.FileType switch
			{
				FileType.Xml => new XmlParser($"{_fileSetting.FileName}.xml"),
				FileType.Csv => new CsvParser($"{_fileSetting.FileName}.csv"),
				_ => throw new ArgumentOutOfRangeException()
			};
		}
	}
}