﻿using System.Collections.Generic;
using System.IO;

using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

using ServiceStack.Text;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
	public class CsvParser : IDataParser<List<Customer>>
	{
		private readonly string _file;

		public CsvParser(string file)
		{
			_file = file;
		}

		public List<Customer> Parse()
		{
			using var reader = new FileStream(_file, FileMode.Open);
			return CsvSerializer.DeserializeFromStream<List<Customer>>(reader);
		}
	}
}